import java.awt.Graphics;
import java.awt.*;
import java.lang.*;
import java.awt.Color;
import java.applet.*;
public class perkotaan extends Applet {
int x,y,w;
void sleep()
{
    try{
        Thread.sleep(200);
    }catch(Exception ob){}
}

public void init(){
    y=350;
    x=20;
}

public void paint(Graphics g) {
    g.drawString("Kenang Ghalih",650,140);
    g.drawString("201943500208 - R7C",650,160);
    //pelangi
    g.setColor(Color.red);
    g.drawArc(0,0,700,850,0,180);
    g.setColor(Color.orange);
    g.drawArc(20,20,650,800,0,180);
    g.setColor(Color.yellow);
    g.drawArc(40,40,600,750,0,180);
    g.setColor(Color.green);
    g.drawArc(60,60,550,700,0,180);
    g.setColor(Color.cyan);
    g.drawArc(80,80,500,650,0,180);
    g.setColor(Color.blue);
    g.drawArc(100,100,450,600,0,180);
    g.setColor(Color.magenta);
    g.drawArc(120,120,400,550,0,180);
    g.setColor(Color.black);
    g.drawArc(140,140,350,500,0,180);
    //warna-warna
    Color a = new Color (250,250,140); //kuning terang
    Color b = new Color (250,140,140); //pink keunguan
    Color c = new Color (140,140,140);//abu 
    Color c1 = new Color(20,160,200); //biru terang
    Color c2 = new Color(200,60,200); //ungu
    Color e[] = {Color.green,Color.green,Color.blue,Color.yellow,Color.red,Color.cyan,Color.black};    
    //matahari
    g.setColor(a);
    g.fillOval(590,20,90,90);
    g.setColor(Color.yellow);
    g.fillOval(600,30,65,65);
    g.setColor(Color.orange);
    g.fillOval(610,40,50,50);
    //gedung A
    g.setColor(Color.orange);
    g.fillRect(20,20,200,405);
    //jendela atas
    for (int i=1;i<=6;i++){
        g.setColor(e[i]);
        g.fillRect(5+i*30,40,20,20);
    }
    g.setColor(Color.black);
    //jendela y kedua
    g.fillRect(35,70,40,20);
    g.fillRect(85,100,30,20);
    g.fillRect(125,100,30,20);
    g.fillRect(165,70,40,20);
    g.fillArc(50,30,140,200,180,180);
    g.setColor(Color.red);
    g.fillArc(70,30,100,200,180,180); 
    g.setColor(b);
    g.fillArc(90,30,60,200,180,180);
    g.setColor(Color.black);    
    for (int i=1;i<=6;i++){
        g.setColor(e[i]);
        g.fillRect(5+i*30,250,20,20);
    }
    //
    g.setColor(Color.cyan);
    for (int i=1;i<=6;i++){     
        g.fillRect(5+i*30,280,20,20);
    }
    //
    g.setColor(b);
    for (int i=1;i<=6;i++){
        g.setColor(e[i]);
        g.fillRect(5+i*30,310,20,20);
    }
    //pintu & jendela
    g.setColor(Color.black);
    g.fillRect(35,340,45,40);
    g.fillRect(90,340,60,85);
    g.fillRect(160,340,45,40);
    g.setColor(Color.white);
    g.fillRect(120,340,5,85);   
    //gedung B
    g.setColor(Color.red);
    g.fillRect(250,100,150,325);
    //jendela atas
    g.setColor(Color.cyan);
    for (int i=1;i<=4;i++){
        g.fillRect(240+i*30,120,20,20);
    }
    //
    g.setColor(Color.black);
    for (int i=1;i<=4;i++){
        g.fillRect(240+i*30,150,20,20);
    }
    //
    g.setColor(Color.cyan);
    for (int i=1;i<=4;i++){
        g.fillRect(240+i*30,150+i*30,20,20);
    }
    //
    g.setColor(b);
    for (int i=1;i<=3;i++){
        g.fillRect(240+i*30,270,20,20);
    }
    //
    g.setColor(Color.black);
    g.fillRect(270,300,20,20);
    g.fillRect(300,300,20,20);
    g.fillRect(330,300,20,20);
    g.fillRect(360,300,20,20);
    //
    g.setColor(c);
    for (int i=1;i<=4;i++){
        g.fillRect(240+i*30,330,20,20);
    }
    //
    g.setColor(Color.cyan);
    g.fillRect(300,360,50,65);
    g.setColor(Color.blue);
    g.fillOval(335,380,10,10);
    //tanah hijau
    g.setColor(Color.green);
    g.fillRect(0,425,1000,90);
    //jalan   
    for (int i=0;i<=20;i++){
        g.setColor(Color.black);
        g.fillRect(0+i*50,450,30,20);
    }
    //rumah
    g.setColor(Color.blue);
    g.drawLine(430,225,500,100);
    g.drawLine(500,100,580,225); 
    g.fillRect(430,225,150,200);
    g.setColor(a);
    g.fillRect(480,275,50,150);
    g.setColor(c);
    g.fillOval(480,160,50,50);
    g.setColor(b);
    g.fillOval(520,350,10,10);

    //bg
    setBackground(Color.pink); 
    w=getWidth();

    g.setColor(c1);
    
    
    g.drawLine(0,y+75,w,y+75);
    g.setColor(c2);
    g.fillRoundRect(x,y+20,100,40,5,5);
    g.fillArc(x+90,y+20,20,40,270,180);
    g.setColor(c1);
    
    g.fillRoundRect(x+10,y,70,25,10,10);
    g.setColor(Color.white);
    g.fillRect(x+20,y+5,20,25);
    g.setColor(Color.black);
    g.fillRoundRect(x+55,y+10,10,20,10,10);
    g.fillOval(x+10,y+50,25,25);
    g.fillOval(x+60,y+50,25,25);
    g.setColor(Color.white);
    g.fillOval(x+15,y+55,10,10);
    g.fillOval(x+65,y+55,10,10);
    
    x+=10;
    sleep();
    
    if(x+100<w){
        repaint();
    }else{
        repaint();
        x=20;
        y=350;
    }
}
}

